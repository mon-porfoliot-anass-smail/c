## C++

# La Bibliothèque std

std est une bibliothèque de directive de préprocesseur, sans cette bibliothèque nous ne pourrions pas
utiliser des directive telle que iostream. Pour l'intégrer a notre code, nous utilisons la commande suivante:

```cpp
using namespace std;
```
la bibliothèque std est la bibliothèque standard elle permet de déclarer des variables et de les afficher.

iostream est une directive de préprocesseur apartenant à std, les directive contienent des fonctions.
Celle de iostream permettent d'afficher des messages et, permet a un utilisateur d'assigner une valeur a une variable.
pour l'utiliser je vais utiliser la commande suivante:

```cpp
#include <iostream>
```

**Important**: iostream ne peut être utiliser uniquement en présence de la bibliothèque std, alors que la bibliothèque peut être intégrer sans insérer iostream mais l'utilisateur ne pourrait pas utiliser les fonctions de iostream.

# Les fonctions de la bibliothèque iostream

cout est une fonction de la directive de préprocesseur iostream elle permet d'afficher un message, ainsi que la valeur d'une variable. Un message seras transcrit entre guillemets, alors que pour la valeur d'une variable, nous devrons alors mettre le nom de la variable sans guillemets.

```cpp
cout << "Ceci est un message!";
cout << maVariable;
```

# Analyse du code

Voici une première partie de code.

```cpp
#include <iostream>

using namespace std;

int main() {
    cout << "Hello world!" << endl;
    return 0;
}
```

**Tout d'abord je peut mettre un commentaire sur mon code qui n'impacteras pas ma solution avec deux //**

**std** peut être appelé de maniére différente cependant plus longue que d'utiliser la méthode using. Il me suffit de placer std:: devant toutes les fonctions provenant des directive contenue dans std.

```cpp
std::cout << "Hello world!" << endl;
```

**int main** est la fonction principale de notre solution, c'est ici que nous créerons nos variables, cout, etc. notre fonction main est de type int, mais nous pouvons lui assigner d'autre type.

**endl** va après l'affichage de notre message, laisser une ligne vide.

**return 0;** en C# cette commande nous permettait de renvoyer dans une fonction une valeur elle remplie le même rôle ici, elle est chargé de nous renvoyer 0 si aucun problème n'a était aperçu sur le code sinon un autre nombre.

pour le code cela causeras une erreur si je rentre ces commandes:
```cpp
cout << "\";
cout << """;
```
si je veux afficher un guillemet ainsi qu'un backslash je dois faire ceci:
```cpp
cout << "\\";
cout << "\"";
```

je peut aussi créer un espace entre deux texte en laissant des guillemets vide dans mon cout
```cpp
cout << "J'ai" << " " << "19 ans";
```

cout avec endl est assez similaire à un Console.WriteLine, 
alors que cout sans à un Console.Write.

# Déclarez une variable

Pour déclarez une variable je dois respecter ces étapes:
lui donner un type
un nom
et une valeur

Voici le tableau des types en C++:

![Alt text](tableauDesTypes.png "Tableau des types")

Pour déclarer une variable je peut m'y prendre de cette maniére:
```cpp
bool majeur(true);
int age(19);
string prénom("Anass");
```

je peut aussi déclarer mes variables de cette façon:
```cpp
bool majeur=true;
int age=19;
string prénom="Anass";
```

je peut aussi commencer à écrire ma variable sans assigner sa valeur et l'assigner à la suite.
```cpp
int age;
age = 19;
``` 

Une variable peut recevoir la valeur d'une autre variable.
```cpp
int agePersonne1(20);
int agePersonne2(agePersonne1);
int agePersonne3 = agePersonne1;
```

La valeur de la premiére variable peut ensuite être réassigné sans impacter les autres variables.
 ```cpp
int agePersonne1(20);
int agePersonne2(agePersonne1);
int agePersonne3 = agePersonne1;

agePersonne1 = 21;
```

nous pouvons faire en sorte qu'une variable change sa valeur lors du moment ou la premiére valeur est réassigné.
```cpp
int agePersonne1(20);
int& agePersonne2(agePersonne1); //Cette variable verras sa valeur modifié si nous modifions celle de la premiére variable
int agePersonne3 = agePersonne1;

agePersonne1 = 21; //Valeur de la variable agePersonne2 aussi modifié
```

Nous ne pouvons pas assigner une valeur d'une autre autre variable si la valeur qui reçoit a déja une valeur.
```cpp
int agePersonne1(24);
int agePersonne2(23);
agePersonne2 = agePersonne1; // erreur dans l'algorithme
```

# cin

cin est l'équivalent de Console.ReadLine() en C++, celle-ci permet à un utilisateur d'entrer lors du déboguage une valeur dans une variable mise en mémoire. Cela marche avec tout types de variables.
```cpp
int ageUtilisateur(0);
cout << "Entrer votre age ? " << endl;
cin >> ageUtilisateur;
string prenomUtilisateur("Sans nom");
cout << "Entrer votre Prénom ? " << endl;
cin >> prenomUtilisateur;
cout << "Mon prénom est " << prenomUtilisateur << ", J'ai " << ageUtilisateur << " ans." << endl;
```

# fonction getline()

Une chaîne de caractère peut avoir des espaces et ce retrouver coupé au mauvais endroit c'est pourquoi la fonction getline() peut rêgler ce soucis.
```cpp
string nomUtilisateur("Sans nom");
getline(cin, nomUtilisateur);
```

# Les opérateurs

Nous pouvons utiliser des variables afin de faire des calculs, pour cela :
```cpp
int numberOne(5);
int numberTwo(10);
int resultat = numberOne + numberTwo;
```

Voici tout les opérateurs de calcul en C++.

![Alt text](tableauDesOperations.png "Tableau des opérations")

Modulo permet de récupérer le reste d'une division.

# variable constante

Une variable constante est une variable ne pouvons pas être modifié, pour la déclaré voici la syntaxte.
```cpp
int const nombreNiveaux(10);
string const nomPersonnage("Lonk");
```
rendre une variable constante permet d'éviter les erreurs d'inatention, et de créer, un programme plus efficace.

# Ajoutez "1" à une variable avec l'incrémentation

Nous pouvons rajouter à une variable 1 de sa valeur, via la syntaxte suivante.
```cpp
int nombreParticipants(9);
++nombreParticipants;
```

# Retirez "1" à une variable avec la décrémentation

Nous pouvons retirez à une variable 1 de sa valeur, via la syntaxte suivante.
```cpp
int nombreParticipants(9);
--nombreParticipants;
```

# Raccourcis opérateurs

pour les opérateurs si nous voulons additioner une variable int à une valeur nous utilisrons logiquement cette syntaxte.
```cpp
int nombreJeux(5);
nombreJeux = nombreJeux + 1;
```

Nous avons un raccourcis à cette variable, qui est la suivante.
```cpp
int nombreJeux(5);
nombreJeux += 1;
```

Voici tout les raccourcis.
```cpp
double nombre(5.3);
nombre += 4.2;
nombre *= 2.;        
nombre -= 1.;        
nombre /= 3.;
```

# Utilisez des fonctions mathématiques

Nous devons dabord entrer la directive suivante.
```cpp
#include <cmath>
```

Une fois fait nous pouvons utiliser des fonction de math telle que la fonction sqrt qui permet d'obtenir la racine d'un nombre.
```cpp
#include <iostream>
#include <cmath>  //Ne pas oublier cette ligne 
using namespace std;

int main()
{
    double const nombre(16); //Le nombre dont on veut la racine

    double resultat;         //Une case mémoire pour stocker le résultat

    resultat = sqrt(nombre);  //On effectue le calcul !

    cout << "La racine de " << nombre << " est " << resultat << endl;

    return 0;
}
```
Voici toutes les fonctions que nous pouvons retrouver avec cette directive.

![Alt text](tableauFonctionsMath.png "Tableau des fonctions de la directive math")

Nous avons aussi la fonction puissance qui permet de calculer un nombre accompagné d'une puissance telle que 4 puissance 5.
```cpp
double const a(4);
double const b(5);
double const resultat = pow(a,b);
```

# Les conditions

dans un code C++ nous allons devoir utiliser des conditions ainsi que des opérateurs de comparaison pour que les programmes puisse prendre des décisions, pour ce faire nous allons travaillé avec les premiéres conditions **if** et **else**. Ainsi que ces opérateurs de comparaison.

![Alt text](tableauDesOpérateursDeComparaison.png "Tableau des opérateurs de comparaison en C++")

**Attention** l'opérateur de comparaison == n'a pas la même signification que = qui est un opérateur de comparaison qui signifie "affecter à".

## La condition if else

la condition **if else** peut ce traduire par Si et Autre, celle-ci va nous permettre de vérifier une condition avec nos variables ainsi que nos opérateurs de comparaison. Nous allons commencer avec un code simple